# Код программы модуля mypass
name = "ivanov"
part = 2

# получаем файл, заполняем шаблон данными
template = open("template.txt", "r" , encoding="utf8")
templ_text = template.read()
result = templ_text.format(t_name=name, t_part=part)
template.close()

# cоздаем новый файл, записываем значениe
file = open("pass_invanov.txt", "w")
file.write(result) 
file.close()
